
SOURCES += \
    $$BSONIO_DIR/nservice.cpp \
    $$BSONIO_DIR/gdatastream.cpp \
    $$BSONIO_DIR/ar2base.cpp \
#    $$BSONIO_DIR/ar2bson.cpp \
#    $$BSONIO_DIR/ar2format.cpp \
#    $$BSONIO_DIR/valsimpl.cpp \
#    $$BSONIO_DIR/valsstl.cpp \
#    $$BSONIO_DIR/n_object.cpp \
#    $$BSONIO_DIR/io2format.cpp \
    $$BSONIO_DIR/v_json.cpp \
    $$BSONIO_DIR/v_yaml.cpp \
    $$BSONIO_DIR/v_xml.cpp \
    $$BSONIO_DIR/json2cfg.cpp \
    $$BSONIO_DIR/dbkeyfields.cpp \
    $$BSONIO_DIR/dbquerydef.cpp \
#    $$BSONIO_DIR/dbbase.cpp \
#    $$BSONIO_DIR/dbschema.cpp \
#    $$BSONIO_DIR/dbgraph.cpp \
    $$BSONIO_DIR/dbdriverejdb.cpp \
    $$BSONIO_DIR/dbserverejdb.cpp \
    $$BSONIO_DIR/dbclient.cpp \
#    $$BSONIO_DIR/nejdb.cpp \
    $$BSONIO_DIR/thrift_schema.cpp \
    $$BSONIO_DIR/thrift_node.cpp \
    $$BSONIO_DIR/thrift_import.cpp \
    $$BSONIO_DIR/TBSONSerializer.cpp \
    $$BSONIO_DIR/lua-run.cpp \
    $$BSONIO_DIR/io_settings.cpp \
    $$BSONIO_DIR/dbconnect.cpp \
    $$BSONIO_DIR/dbcollection.cpp \
    $$BSONIO_DIR/dbdocument.cpp \
    $$BSONIO_DIR/dbschemadoc.cpp \
    $$BSONIO_DIR/dbvertexdoc.cpp \
    $$BSONIO_DIR/dbedgedoc.cpp \
    $$BSONIO_DIR/traversal.cpp

HEADERS  += \
    $$BSONIO_DIR/nservice.h \
    $$BSONIO_DIR/gdatastream.h \
    $$BSONIO_DIR/ar2base.h \
#    $$BSONIO_DIR/ar2bson.h \
#    $$BSONIO_DIR/ar2format.h \
#    $$BSONIO_DIR/valsbase.h \
#    $$BSONIO_DIR/valsimpl.h \
#    $$BSONIO_DIR/valsstl.h \
#    $$BSONIO_DIR/n_object.h \
#    $$BSONIO_DIR/io2format.h \
    $$BSONIO_DIR/v_json.h \
    $$BSONIO_DIR/v_yaml.h \
    $$BSONIO_DIR/v_xml.h \
    $$BSONIO_DIR/json2cfg.h \
    $$BSONIO_DIR/dbkeyfields.h \
    $$BSONIO_DIR/dbquerydef.h \
#    $$BSONIO_DIR/dbbase.h \
#    $$BSONIO_DIR/dbschema.h \
#    $$BSONIO_DIR/dbgraph.h \
    $$BSONIO_DIR/dbdriverejdb.h \
    $$BSONIO_DIR/dbserverejdb.h \
    $$BSONIO_DIR/dbclient.h \
#    $$BSONIO_DIR/nejdb.h \
    $$BSONIO_DIR/thrift_schema.h \
    $$BSONIO_DIR/thrift_node.h \
    $$BSONIO_DIR/thrift_import.h \
    $$BSONIO_DIR/TBSONSerializer.h \
    $$BSONIO_DIR/lua-run.h \
    $$BSONIO_DIR/io_settings.h \
    $$BSONIO_DIR/dbcollection.h \
    $$BSONIO_DIR/dbconnect.h \
    $$BSONIO_DIR/dbdocument.h \
    $$BSONIO_DIR/dbschemadoc.h \
    $$BSONIO_DIR/dbvertexdoc.h \
    $$BSONIO_DIR/dbedgedoc.h \
    $$BSONIO_DIR/traversal.h \

