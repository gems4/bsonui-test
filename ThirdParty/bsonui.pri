
SOURCES += \
    $$BSONUI_DIR/model_bson.cpp \
    $$BSONUI_DIR/model_schema.cpp \
    $$BSONUI_DIR/model_table.cpp \
    $$BSONUI_DIR/model_node.cpp \
    $$BSONUI_DIR/model_query.cpp \
    $$BSONUI_DIR/preferences.cpp \
    $$BSONUI_DIR/PreferencesBSONUI.cpp \
    $$BSONUI_DIR/BSONUIBase.cpp \
    $$BSONUI_DIR/BSONUIWidget.cpp \
    $$BSONUI_DIR/VertexWidget.cpp \
    $$BSONUI_DIR/VertexMenu.cpp \
    $$BSONUI_DIR/EdgesWidget.cpp \
    $$BSONUI_DIR/EdgesMenu.cpp \
    $$BSONUI_DIR/QueryWidget.cpp \
    $$BSONUI_DIR/FormatImpexWidget.cpp \
    $$BSONUI_DIR/TableEditWindow.cpp \
    $$BSONUI_SERVICE_DIR/TBsonProtocol.cpp \
    $$BSONUI_SERVICE_DIR/SelectDialog.cpp \
    $$BSONUI_SERVICE_DIR/CalcDialog.cpp \
    $$BSONUI_SERVICE_DIR/ModelLineDialog.cpp \
    $$BSONUI_SERVICE_DIR/SchemaSelectDialog.cpp \
    $$BSONUI_SERVICE_DIR/dbkeysmodel.cpp \
    $$BSONUI_SERVICE_DIR/TCSVPage.cpp \
    $$BSONUI_PLOTS_DIR/graph_data.cpp \
    $$BSONUI_PLOTS_DIR/SymbolDialog.cpp \
    $$BSONUI_PLOTS_DIR/LegendDialog.cpp \
    $$BSONUI_PLOTS_DIR/GraphDialog.cpp \
    $$BSONUI_PLOTS_DIR/markershapes.cpp \
    $$BSONUI_PLOTS_DIR/chart_model.cpp \
    $$BSONUI_PLOTS_DIR/chart_view.cpp \
    $$BSONUI_HELP_DIR/helpdata.cpp \
    $$BSONUI_HELP_DIR/HelpMainWindow.cpp \
    $$BSONUI_HELP_DIR/helpgenerator.cpp

HEADERS += \
    $$BSONUI_DIR/model_bson.h \
    $$BSONUI_DIR/model_schema.h \
    $$BSONUI_DIR/model_table.h \
    $$BSONUI_DIR/model_node.h \
    $$BSONUI_DIR/model_query.h \
    $$BSONUI_DIR/preferences.h \
    $$BSONUI_DIR/PreferencesBSONUI.h \
    $$BSONUI_DIR/BSONUIBase.h \
    $$BSONUI_DIR/BSONUIWidget.h \
    $$BSONUI_DIR/VertexWidget.h \
    $$BSONUI_DIR/EdgesWidget.h \
    $$BSONUI_DIR/QueryWidget.h \
    $$BSONUI_DIR/FormatImpexWidget.h \
    $$BSONUI_DIR/TableEditWindow.h \
    $$BSONUI_SERVICE_DIR/TBsonProtocol.h \
    $$BSONUI_SERVICE_DIR/SelectDialog.h \
    $$BSONUI_SERVICE_DIR/CalcDialog.h \
    $$BSONUI_SERVICE_DIR/ModelLineDialog.h \
    $$BSONUI_SERVICE_DIR/SchemaSelectDialog.h \
    $$BSONUI_SERVICE_DIR/dbkeysmodel.h \
    $$BSONUI_SERVICE_DIR/TCSVPage.h \
    $$BSONUI_PLOTS_DIR/graph_data.h \
    $$BSONUI_PLOTS_DIR/GraphDialog.h \
    $$BSONUI_PLOTS_DIR/LegendDialog.h \
    $$BSONUI_PLOTS_DIR/SymbolDialog.h \
    $$BSONUI_PLOTS_DIR/markershapes.h \
    $$BSONUI_PLOTS_DIR/chart_model.h \
    $$BSONUI_PLOTS_DIR/chart_view.h \
    $$BSONUI_HELP_DIR/helpdata.h \
    $$BSONUI_HELP_DIR/helpgenerator.h \
    $$BSONUI_HELP_DIR/HelpMainWindow.h

FORMS += \
    $$BSONUI_SERVICE_DIR/SelectDialog4.ui \
    $$BSONUI_SERVICE_DIR/CalcDialog4.ui \
    $$BSONUI_SERVICE_DIR/ModelLineDialog.ui \
    $$BSONUI_SERVICE_DIR/SchemaSelectDialog4.ui \
    $$BSONUI_DIR/PreferencesBSONUI.ui \
    $$BSONUI_DIR/BSONUIWidget.ui \
    $$BSONUI_DIR/VertexWidget.ui \
    $$BSONUI_DIR/EdgesWidget.ui \
    $$BSONUI_DIR/QueryWidget.ui \
    $$BSONUI_DIR/FormatImpexWidget.ui \
    $$BSONUI_DIR/TableEditWindow.ui \
    $$BSONUI_PLOTS_DIR/GraphDialog4.ui \
    $$BSONUI_PLOTS_DIR/LegendDialog4.ui \
    $$BSONUI_PLOTS_DIR/SymbolDialog4.ui \
    $$BSONUI_HELP_DIR/HelpMainWindow.ui
