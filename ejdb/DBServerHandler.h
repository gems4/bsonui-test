#ifndef DBSERVERHANDLER_H
#define DBSERVERHANDLER_H

#include "DBServer.h"
#include <thrift/protocol/TBinaryProtocol.h>
#include <thrift/server/TSimpleServer.h>
#include <thrift/transport/TServerSocket.h>
#include <thrift/transport/TBufferTransports.h>

using namespace ::apache::thrift;
using namespace ::apache::thrift::protocol;
using namespace ::apache::thrift::transport;
using namespace ::apache::thrift::server;

using boost::shared_ptr;
using namespace  ::dbserver;

#include "nejdb.h"
using namespace bsonio;

class DBServerHandler : virtual public DBServerIf
{

    // will be mutex for collecs_ amd map of mutexes for all collections

    /// Directory to store db files.
    std::string dbName_;
    /// map of collections
    std::map<std::string, boost::shared_ptr<bsonio::TEJDBString> > collecs_;

public:

  explicit DBServerHandler( const char* pathname ):dbName_(pathname)
  {
     setDB( pathname );
  }

  /// Open/Create EJDB for path
  void setDB(const char* pathname  )
  {
     bsonio::flEJ.reOpen( pathname );
  }

  /**
   * Pings DBServer.
   *
   * @return Success - if ping was successful.
   *         Error -   if ping failed.
   */
  ResponseCode::type ping();

  /**
   * Add a new collection of records.
   *
   * A record is a string key / json string value pair.
   * A key uniquely identifies a record in a collection.
   *
   * @param  colName collection name
   *         keynames list of names used as key fields
   * @return Success - on success.
   *         NameExists - collection already exists.
   *         Error - on any other errors.
   */
  ResponseCode::type addCollection(const std::string& colName,
                                   const std::vector<std::string> & keynames);

  /**
   * Drops a collection of records.
   *
   * @param colName collection name
   * @return Ok - on success.
   *         NameNotFound - map doesn't exist.
   *         Error - on any other errors.
   */
  ResponseCode::type dropCollection(const std::string& colName);

  /**
   * List all the collections.
   *
   * @returns StringListResponse
   *              responseCode Success  or
   *                           Error - on error.
   *              values - list of collection names.
   */
  void listCollections(StringListResponse& _return);

  /**
   * Add new record into the collection
   *
   * @param colName       collection name
   * @param value         record value to save
   * @returns
   *          responseCode - Success
   *                         NameNotFound collection doesn't exist.
   *                         RecordExists
   *                         Error
   *              value -   oid  of the record.
   */
  void CreateRecord(ValueResponse& _return,
                    const std::string& colName, const std::string& value);

  /**
   * Retrive one record from the collection
   *
   * @param colName       collection name
   * @param key           records key to retrive.
   * @returns BinaryResponse
   *              responseCode - Success
   *                             NameNotFound collection doesn't exist.
   *                             RecordNotFound record doesn't exist.
   *                             Error on any other errors.
   *              value - value of the record.
   */
  void ReadRecord(ValueResponse& _return,
                  const std::string& colName, const std::string& key);

  /**
   * Update record in the collection
   *
   * @param colName       collection name
   * @param key           records key to update.
   * @param value         record value to update
   * @returns Success
   *          NameNotFound collection doesn't exist.
   *          RecordNotFound
   *          Error
   */
  ResponseCode::type UpdateRecord(const std::string& colName,
                      const std::string& key, const std::string& value);

  /**
   * Removes record from the collection
   *
   * @param colName       collection name
   * @param key           records key to delete.
   * @returns Success
   *          NameNotFound collection doesn't exist.
   *          RecordNotFound
   *          Error
   */
  ResponseCode::type DeleteRecord(const std::string& colName, const std::string& key);

  /**
   * Returns list of records in a collection.
   *
   * @param colName       collection name
   * @param recordQuery   json query string for collection
   * @return RecordListResponse
   *             responseCode - Success
   *                          - NameNotFound collection doesn't exist.
   *                          - Error on any other errors
   *             records - list of records.
   */
  void SearchRecords(DBRecordListResponse& _return, const std::string& colName,
                     const std::string& recordQuery);

  /**
   * Returns list of records keys in a collection.
   *
   * @param colName       collection name
   * @param recordQuery   json query string for collection
   * @return RecordListResponse
   *             responseCode - Success
   *                          - NameNotFound collection doesn't exist.
   *                          - Error on any other errors
   *             keys - list of keys.
   */
  void SearchKeys(StringListResponse& _return, const std::string& colName,
                  const std::string& recordQuery);

  /**
   * Find record with key into the collection
   *
   * @param colName       collection name
   * @param key           records key to test.
   * @returns BinaryResponse
   *              responseCode - Success
   *                             NameNotFound database doesn't exist.
   *                             Error on any other errors.
   *              value - oid of the record.
  */
  void FindRecord(ValueResponse& _return, const std::string& colName, const std::string& key) ;

};


#endif // DBSERVERHANDLER_H
