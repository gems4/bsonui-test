//-------------------------------------------------------------------
// $Id: PreferencesDialog.cpp 333 2014-03-13 13:23:32Z gemsfits $
//
// Implementation of ServerPreferences class
//
// Copyright (C) 2014  S.V.Dmytriyeva
// Uses Qwt (http://qwt.sourceforge.net), EJDB (http://ejdb.org),
//    yaml-cpp (https://code.google.com/p/yaml-cpp/)
//
// This file is part of the GEMSFITS GUI, which uses the
// Qt v.5 cross-platform App & UI framework (http://qt-project.org)
// under LGPL v.2.1 (http://www.gnu.org/licenses/lgpl-2.1.html)
//
// This file may be distributed under the terms of LGPL v.3 license
//
// See http://gems.web.psi.ch/GEMSFIT for more information
// E-mail gems2.support@psi.ch
//-------------------------------------------------------------------

#include <QFileDialog>
#include "ServerPreferences.h"
#include "ui_ServerPreferences.h"

ServerPreferences::ServerPreferences(QSettings *aSet,QWidget *parent) :
    QDialog(parent),settings(aSet),
    ui(new Ui::ServerPreferences)
{
    ui->setupUi(this);
    if( settings ) //load old settings
    {
        ui->resourcesEdit->setText( settings->value("DBPath", ".").toString() );
        ui->usersEdit->setText( settings->value("UserFolderPath", ".").toString() );
//        ui->commentBox->setChecked( settings->value("ShowComments", false).toBool() );
        ui->spinBox->setValue(settings->value("MaxMessages", 300).toInt());
    }

    QObject::connect( ui->buttonBox, SIGNAL(accepted()), this, SLOT(CmSave()));
    QObject::connect( ui->buttonBox, SIGNAL(helpRequested()), this, SLOT(CmHelp()));
    QObject::connect( ui->usersButton, SIGNAL(clicked()), this, SLOT(CmProjectDir()));
    QObject::connect( ui->resourcesButton, SIGNAL(clicked()), this, SLOT(CmDBDir()));
}

ServerPreferences::~ServerPreferences()
{
    delete ui;
}

void ServerPreferences::CmSave()
{
    if( !settings )
       return;

    settings->setValue("DBPath", ui->resourcesEdit->text() );
    settings->setValue("UserFolderPath", ui->usersEdit->text() );
    //settings->setValue("ShowComments",  ui->commentBox->isChecked() );
    settings->setValue("MaxMessages",  ui->spinBox->value() );
    ui->spinBox->setValue(settings->value("MaxMessages", 300).toInt());
    settings->sync();
    accept();
}

void ServerPreferences::CmProjectDir()
{
    QString dir = QFileDialog::getExistingDirectory(this, "Select Project Directory",
     "",  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );
    ui->usersEdit->setText( dir );
}

void ServerPreferences::CmDBDir()
{
    QString dir = QFileDialog::getExistingDirectory(this, "Select start DB Directory",
     "",  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );
    ui->resourcesEdit->setText( dir );
}

void ServerPreferences::CmHelp()
{ }


// ------------------------ end of ServerPreferences.cpp ------------------------------------------
