#include "DBServerHandler.h"


TSimpleServer* makeServer( const char *path )
{
  int port = 9090;
  boost::shared_ptr<DBServerHandler> handler(new DBServerHandler(path));
  boost::shared_ptr<TProcessor> processor(new DBServerProcessor(handler));
  boost::shared_ptr<TServerTransport> serverTransport(new TServerSocket(port));
  boost::shared_ptr<TTransportFactory> transportFactory(new TBufferedTransportFactory());
  boost::shared_ptr<TProtocolFactory> protocolFactory(new TBinaryProtocolFactory());

  return new TSimpleServer(processor, serverTransport, transportFactory, protocolFactory);
}

ResponseCode::type DBServerHandler::ping()
{
  printf("ping\n");
  return ResponseCode::Success;
}

ResponseCode::type DBServerHandler::addCollection(const std::string& colName,
             const std::vector<std::string> & keynames)
{
  printf("addCollection\n");
  //boost::unique_lock< boost::shared_mutex> writeLock(mutex_);;
  std::map<std::string, boost::shared_ptr<TEJDBString> >::iterator itr = collecs_.find(colName);
  if (itr != collecs_.end())
      return ResponseCode::NameExists;

  // default key information
  vector<KeyFldsData> keyFldsInf;
  auto itname = keynames.begin();
  while( itname !=  keynames.end() )
     keyFldsInf.push_back( KeyFldsData(  *itname, *itname++ ) );

  // init new DB collection
  boost::shared_ptr<TEJDBString> ejdb( new TEJDBString( colName.c_str(), keyFldsInf  ));
  collecs_.insert(pair<string,boost::shared_ptr<TEJDBString> >( colName, ejdb ));
  return ResponseCode::Success;
}

ResponseCode::type DBServerHandler::dropCollection(const std::string& colName)
{
  printf("dropCollection\n");
  // boost::unique_lock< boost::shared_mutex> writeLock(mutex_);;
  std::map<std::string, boost::shared_ptr<TEJDBString> >::iterator itr = collecs_.find(colName);
  if (itr == collecs_.end())
         return ResponseCode::NameNotFound;

  try{
       itr->second.get()->Close();
       collecs_.erase(itr);
       // remove path??.
       //unlink((dbName_ + "/" + colName).c_str());
     }
   catch( ... ) {
       return ResponseCode::Error;
     }
   return ResponseCode::Success;
}

void DBServerHandler::listCollections(StringListResponse& _return)
{
  printf("listCollections\n");
  // boost::unique_lock< boost::shared_mutex> writeLock(mutex_);;
  std::map<std::string, boost::shared_ptr<TEJDBString> >::iterator itr = collecs_.begin();
  while(itr != collecs_.end())
     _return.keys.push_back( itr++->first );
 _return.responseCode = ResponseCode::Success;
}

void DBServerHandler::CreateRecord(ValueResponse& _return,
          const std::string& colName, const std::string& value)
{
  printf("CreateRecord\n");
  // boost::shared_lock< boost::shared_mutex> readLock(mutex_);; must lock mutex for current collection
  std::map<std::string, boost::shared_ptr<TEJDBString> >::iterator itr = collecs_.find(colName);
  if (itr == collecs_.end())
  {   _return.responseCode = ResponseCode::NameNotFound;
      return;
  }
  try{  // set internal data to DB
         itr->second.get()->SetJsonYaml( value, true );
         // save internal data to DB
         itr->second.get()->InsertRecord();
         _return.value = itr->second.get()->currentOid() ;
         _return.responseCode = ResponseCode::Success;
      }
  catch(bsonio_exeption& e)
  {
     if( e.title() == string("TEJDB0004") )
       _return.responseCode = ResponseCode::RecordExists;
     else
       _return.responseCode = ResponseCode::Error;
   }
   catch( ... )
    {
       _return.responseCode = ResponseCode::Error;
    }
}

void DBServerHandler::ReadRecord(ValueResponse& _return,
       const std::string& colName, const std::string& key)
{
  printf("ReadRecord\n");
  // boost::shared_lock< boost::shared_mutex> readLock(mutex_);; must lock mutex for current collection
   std::map<std::string, boost::shared_ptr<TEJDBString> >::iterator itr = collecs_.find(colName);
   if (itr == collecs_.end())
   {   _return.responseCode = ResponseCode::NameNotFound;
       return;
   }

   try{   // read record from DB
          itr->second.get()->Get( key.c_str() );
          _return.value =  itr->second.get()->GetJson();
          _return.responseCode = ResponseCode::Success;
      }
   catch(bsonio_exeption& e)
   {
       if( e.title() == string("TEJDB0001") )
           _return.responseCode = ResponseCode::RecordNotFound;
       else
          _return.responseCode = ResponseCode::Error;
   }
   catch( ... )
    {
       _return.responseCode = ResponseCode::Error;
    }
}

ResponseCode::type DBServerHandler::UpdateRecord(const std::string& colName,
         const std::string& key, const std::string& value)
{
  printf("UpdateRecord\n");
  // boost::shared_lock< boost::shared_mutex> readLock(mutex_);; must lock mutex for current collection
  std::map<std::string, boost::shared_ptr<TEJDBString> >::iterator itr = collecs_.find(colName);
  if (itr == collecs_.end())
       return ResponseCode::NameNotFound;

  try{  // test record exist
        if( !itr->second.get()->Find( key.c_str()) )
             return ResponseCode::RecordNotFound;
         // set internal data to DB
         itr->second.get()->SetJsonYaml( value, true );
         // save internal data to DB
         itr->second.get()->SaveRecord( key.c_str() );
      }
   catch(bsonio_exeption& e)
   {
       return ResponseCode::Error;
   }
   catch( ... )
    {
       return ResponseCode::Error;
    }

   return ResponseCode::Success;

}

ResponseCode::type DBServerHandler::DeleteRecord(const std::string& colName, const std::string& key)
{
  printf("DeleteRecord\n");
  // boost::shared_lock< boost::shared_mutex> readLock(mutex_);; must lock mutex for current collection
  std::map<std::string, boost::shared_ptr<TEJDBString> >::iterator itr = collecs_.find(colName);
  if (itr == collecs_.end())
      return ResponseCode::NameNotFound;

  try{
        itr->second.get()->Del( key.c_str() );
     }
  catch(bsonio_exeption& e)
  {
    if( e.title() == string("TEJDB0002") )
      return ResponseCode::RecordNotFound;
    else
       return ResponseCode::Error;
  }
  catch( ... )
   {
       return ResponseCode::Error;
   }

  return ResponseCode::Success;
}


void DBServerHandler::SearchRecords(DBRecordListResponse& _return,
               const std::string& colName, const std::string& recordQuery)
{
  printf("SearchRecords\n");
  // boost::shared_lock< boost::shared_mutex> readLock(mutex_);; must lock mutex for current collection
   std::map<std::string, boost::shared_ptr<TEJDBString> >::iterator itr = collecs_.find(colName);
   if (itr == collecs_.end())
   {   _return.responseCode = ResponseCode::NameNotFound;
        return;
   }

   try{   if( !recordQuery.empty() )
          { // close collection
            itr->second.get()->Close();
            // reopen with query
            itr->second.get()->SetQueryJson( recordQuery );
            itr->second.get()->Open();
          }
          // get keys by keyfilter
          std::string  keyFilter_ = "*";
          vector<string> aKeyList;
          // Get key list for a wildcard search
          itr->second.get()->GetKeyList( keyFilter_.c_str(), aKeyList, false );

          // read records
          for( uint ii=0; ii<aKeyList.size(); ii++ )
          {
              itr->second.get()->Get(aKeyList[ii].c_str());
              DBRecord rec;
              rec.key = aKeyList[ii];
              rec.value = itr->second.get()->GetJson();
              _return.records.push_back( rec );
          }

          if( !recordQuery.empty() )
          { // close collection
            itr->second.get()->Close();
            // reopen without query
            itr->second.get()->SetQueryJson( "" );
            itr->second.get()->Open();
          }
         _return.responseCode = ResponseCode::Success;

   }
   catch( ...)
   {
     _return.responseCode = ResponseCode::Error;
   }
}

void DBServerHandler::SearchKeys(StringListResponse& _return, const std::string& colName,
                const std::string& recordQuery)
{
  printf("SearchKeys\n");
  // boost::shared_lock< boost::shared_mutex> readLock(mutex_);; must lock mutex for current collection
  std::map<std::string, boost::shared_ptr<TEJDBString> >::iterator itr = collecs_.find(colName);
  if (itr == collecs_.end())
  {   _return.responseCode = ResponseCode::NameNotFound;
       return;
  }

  try{   if( !recordQuery.empty() )
         { // close collection
            itr->second.get()->Close();
            // reopen with query
            itr->second.get()->SetQueryJson( recordQuery );
            itr->second.get()->Open();
          }

         // Get key list for a wildcard search
         vector<string> aKeyList;
         itr->second.get()->GetKeyList( "*", aKeyList, false );

         for( uint ii=0; ii<aKeyList.size(); ii++ )
              _return.keys.push_back(aKeyList[ii]);

          if( !recordQuery.empty() )
          { // close collection
            itr->second.get()->Close();
            // reopen without query
            itr->second.get()->SetQueryJson( "" );
            itr->second.get()->Open();
          }
         _return.responseCode = ResponseCode::Success;
      }
   catch(...)
   {
     _return.responseCode = ResponseCode::Error;
   }
}

void DBServerHandler::FindRecord(ValueResponse& _return,
            const std::string& colName, const std::string& key )
{
  printf("FindRecord\n");
  // boost::shared_lock< boost::shared_mutex> readLock(mutex_);; must lock mutex for current collection
  std::map<std::string, boost::shared_ptr<TEJDBString> >::iterator itr = collecs_.find(colName);
  if (itr == collecs_.end())
   {   _return.responseCode = ResponseCode::NameNotFound;
       return;
   }

   try{   // test record from DB
          _return.value =  itr->second.get()->findOid( key.c_str() );
          _return.responseCode = ResponseCode::Success;
      }
   catch( ...)
   {
     _return.responseCode = ResponseCode::Error;
   }
}
