#include <QFile>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>

#include "ServerWindow.h"
#include "ui_ServerWindow.h"
#include "json2cfg.h"
#include "model_schema.h"
#include "SelectDialog.h"
#include "ServerPreferences.h"
using namespace bsonio;

void ServerWindow::setDefValues()
{
   // load main programm settingth
   mainSettings = new QSettings("ejdb-server.ini", QSettings::IniFormat);
   getDataFromPreferences();
}

void ServerWindow::getDataFromPreferences()
{
  if( !mainSettings)
   return;
  DBDir =  mainSettings->value("DBPath", ".").toString();
  UserDir = mainSettings->value("UserFolderPath", ".").toString();
  ui->appOutput->setMaximumBlockCount(mainSettings->value("MaxMessages", 300).toInt());

}


ServerWindow::ServerWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DBMainWindow)
{

   ui->setupUi(this);
   //set up main parameters
   setDefValues();
   setActions();
}

ServerWindow::~ServerWindow()
{
    delete ui;
}

//  Connect all actions
void ServerWindow::setActions()
{
 // Tasks
    connect( ui->actionOpen_DB, SIGNAL( triggered()), this, SLOT(CmOpenDB()));
    connect( ui->action_Create_New, SIGNAL( triggered()), this, SLOT(CmCreateDB()));
    connect( ui->actionStart_DB_server, SIGNAL( triggered()), this, SLOT(CmStartServer()));
    connect( ui->actionStop_DB_server, SIGNAL( triggered()), this, SLOT(CmStopServer()));
    connect( ui->actionE_xit, SIGNAL( triggered()), this, SLOT(close()));

 // Help
    connect( ui->action_Help_About, SIGNAL( triggered()), this, SLOT(CmHelpAbout()));
    connect( ui->actionAuthors, SIGNAL( triggered()), this, SLOT(CmHelpAuthors()));
    connect( ui->actionLicense, SIGNAL( triggered()), this, SLOT(CmHelpLicense()));
    connect( ui->actionPreferences, SIGNAL( triggered()), this, SLOT(CmSettingth()));

 // Record list
    connect( ui->actionExport_Json_File, SIGNAL( triggered()), this, SLOT(CmExportJSON()));
    connect( ui->actionImport_Json_File, SIGNAL( triggered()), this, SLOT(CmImportJSON()));
    connect( ui->action_Export_YAML_File, SIGNAL( triggered()), this, SLOT(CmExportYAML()));
    connect( ui->actionImport_YAML_File, SIGNAL( triggered()), this, SLOT(CmImportYAML()));
    connect( ui->actionExport_XML_File, SIGNAL( triggered()), this, SLOT(CmExportXML()));
    connect( ui->actionImport_XML_File, SIGNAL( triggered()), this, SLOT(CmImportXML()));

}

void ServerWindow::CmSettingth()
{
  try
  {
    // define new preferences
     ServerPreferences dlg(mainSettings);
      if( !dlg.exec() )
          return;
    //get data from settings
    getDataFromPreferences();
  }
  catch(std::exception& e)
  {
        QMessageBox::critical( this, "CmSettingth", e.what() );
  }
}


/*/ Read bson record from json file fileName
void ServerWindow::CmCreate()
{
  try{
          bson_destroy( &curRecord );
          bson_init( &curRecord );
          bson_finish( &curRecord );
          resetBson();
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}
*/

/// Read bson record from json file fileName
void ServerWindow::CmImportJSON()
{
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select file with json objects", "*.json"  ))
       {
          FJson file( fileName);
          file.setType( FileTypes::Json_ );
          bson_destroy( &curRecord );
          file.LoadBson( &curRecord );
       }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to json file fileName
void ServerWindow::CmExportJSON()
{
   try {
         string fileName;
         if(  ChooseFileSave( this, fileName,
                     "Please, select file to backup", "*.json"  ))
         {
            FJson file( fileName);
            file.setType( FileTypes::Json_ );
            file.SaveBson( curRecord.data );
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read bson record from yaml file fileName
void ServerWindow::CmImportYAML()
{
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select file with YAML objects", "*.yaml"  ))
       {
          FJson file( fileName);
          file.setType( FileTypes::Yaml_ );
          bson_destroy( &curRecord );
          file.LoadBson( &curRecord );
       }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to YAML file fileName
void ServerWindow::CmExportYAML()
{
   try {
         string fileName;
         if(  ChooseFileSave( this, fileName,
                     "Please, select file to backup", "*.yaml"  ))
         {
            FJson file( fileName);
            file.setType( FileTypes::Yaml_ );
            file.SaveBson( curRecord.data );
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Read bson record from xml file fileName
void ServerWindow::CmImportXML()
{
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select file with xml objects", "*.xml"  ))
       {
          FJson file( fileName);
          file.setType( FileTypes::XML_ );
          bson_destroy( &curRecord );
          file.LoadBson( &curRecord );
       }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to file fileName
void ServerWindow::CmExportXML()
{
   try {
         string fileName;
         if(  ChooseFileSave( this, fileName,
                     "Please, select file to backup", "*.xml"  ))
         {
            FJson file( fileName);
            file.setType( FileTypes::XML_ );
            file.SaveBson( curRecord.data );
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


//-----------------------------------------------------


void ServerWindow::ChooseCollection(QWidget* par )
{
   curSchemaName = "";
   vector<string> sch_lst = schema.getStructsList();
   if( !sch_lst.empty() )
   { SelectDialog selDlg( par, "Please, select schema for bson data", sch_lst );
     if( selDlg.exec() )
         curSchemaName = selDlg.selString();
   }
}

bool ServerWindow::ChooseFileOpen(QWidget* par, string& path_,
          const char* title, const char *filter )
{
    QString path;
    if( path_.find('/') == string::npos )
           path  =  UserDir+"/"+path_.c_str();
         else
           path = path_.c_str();

    QString filt;
    if( filter )
        filt = QString("Text files (%1);;All files (*)").arg(filter);
    else
        filt = "All files (*)";

    QString fn = QFileDialog::getOpenFileName(  par, title,
          path, filt, 0,
          QFileDialog::DontConfirmOverwrite );
#ifdef buildWIN32
    std::replace( fn.begin(), fn.end(), '/', '\\');
#endif
   if ( !fn.isEmpty() )
    {
       QFileInfo flinfo(fn);
       UserDir = flinfo.dir().path();
       path_ = fn.toUtf8().data();
       return true;
    }
    else
    {
        path_ = "";
        return false;
    }

}

bool ServerWindow::ChooseFileSave(QWidget* par, string& path_,
       const char* title, const char *filter)
{
     QString path;
     if( path_.find('/') == string::npos )
       path  =  UserDir+"/"+path_.c_str();
     else
       path = path_.c_str();

    //replace(path.begin(), path.end(),'\\', '/');

    QStringList filt;
    if( filter )
        filt += QString("Files (%1)").arg(filter);
    else
        filt += "Text files (*.txt)";

    filt += "All files (*)";

    QString selectedFilter;
    QString fn = QFileDialog::getSaveFileName( par, title,
         path, filt.join( ";;" ), &selectedFilter,
         QFileDialog::DontConfirmOverwrite);

    if ( !fn.isEmpty() )
    {
        QFileInfo flinfo(fn);
        UserDir = flinfo.dir().path();
        if( flinfo.suffix().isEmpty() ) // solwing for linux
        {
          int posb = selectedFilter.lastIndexOf(".");
          int pose = selectedFilter.indexOf(")", posb);
          QString ext = selectedFilter.mid(posb+1, pose-posb-1);
          fn += "."+ext;
        }
        path_ = fn.toUtf8().data();
        return true;
    }
    else
    {
        path_  = "";
        return false;
    }
}
