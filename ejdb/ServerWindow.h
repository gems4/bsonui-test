#ifndef TMAINWINDOW2_H
#define TMAINWINDOW2_H

#include <string>
#include <QMainWindow>
#include <QSettings>
using namespace std;

#include "thrift_schema.h"
#include "v_json.h"
#include "model_bson.h"

namespace Ui {
class DBMainWindow;
}

class ServerWindow : public QMainWindow
{
    Q_OBJECT

    // Internal settings
    QSettings *mainSettings;        ///< Properties for program
    QString DBDir;                  ///< Path to data base directory
    QString UserDir;                ///< Path to User directory

    // Internal data
    ThriftSchema schema;
    string curSchemaName = "";
    bson curRecord;

    // Work functions
    void setDefValues();
    void getDataFromPreferences();
    void setActions();

     // Internal dialogs
    bool ChooseFileOpen(QWidget* par, string& path_,
              const char* title, const char *filter );
    bool ChooseFileSave(QWidget* par, string& path_,
           const char* title, const char *filter );
    void ChooseCollection(QWidget* par );


public slots:

    // Tasks
    void CmOpenDB(){}
    void CmCreateDB(){}
    void CmStartServer(){}
    void CmStopServer(){}

    // Help
    void CmHelpAbout(){}
    void CmHelpAuthors(){}
    void CmHelpLicense(){}
    void CmSettingth();

    // File
    void CmExportJSON();
    void CmImportJSON();
    void CmExportYAML();
    void CmImportYAML();
    void CmExportXML();
    void CmImportXML();

public:
    explicit ServerWindow(QWidget *parent = 0);
    ~ServerWindow();

private:
    Ui::DBMainWindow *ui;


};

#endif // TMAINWINDOW2_H
