#-------------------------------------------------
#
# Project created by QtCreator 2016-02-08T16:01:53
#
#-------------------------------------------------

QT   += core gui widgets
QT   += svg printsupport concurrent
QT   += charts webenginewidgets

TARGET = bsonui-test
TEMPLATE = app

CONFIG += thread
CONFIG += c++11

DEFINES += addBSONIO

!win32 {
  DEFINES += __unix
}

macx-g++ {
  DEFINES += __APPLE__
}

macx-clang {
  DEFINES += __APPLE__
  CONFIG -= warn_on
  CONFIG += warn_off
  INCLUDEPATH   += "/usr/local/include"
  DEPENDPATH   += "/usr/local/include"
  LIBPATH += "/usr/local/lib/"
  LIBS += -llua
}
else {

# Define the directory where the gui, third-party libs, resources are located
BUILD_DIR = $$OUT_PWD/..

# Define the directory where the third-party libraries have been installed
#THIRDPARTY_DIR = $$BUILD_DIR/thirdparty/debug
CONFIG(release, debug|release): THIRDPARTY_DIR = $$BUILD_DIR/release/thirdparty
CONFIG(debug, debug|release): THIRDPARTY_DIR = $$BUILD_DIR/debug/thirdparty
# Define the directories where the headers of the third-party libraries have been installed
THIRDPARTY_INCLUDE_DIR = $$THIRDPARTY_DIR/include
# Define the directories where the THIRDPARTY libraries have been installed
THIRDPARTY_LIBRARY_DIR1 = $$THIRDPARTY_DIR/lib

DEPENDPATH   += $$THIRDPARTY_INCLUDE_DIR
INCLUDEPATH   += $$THIRDPARTY_INCLUDE_DIR
LIBS += -L$$THIRDPARTY_LIBRARY_DIR1

  LIBS += -llua5.3
}

# Define the directory where CuteMarkEd code is located
#INCLUDEPATH   += "/usr/local/include/app-static"
#DEPENDPATH   += "/usr/local/include/app-static"

# Define the directory where source code is located
THRIFT_DIR    = ./thrift
SRC_CPP       = ./src
SRC_H   =  $$SRC_CPP

# Define the directory where bsonio source code is located
BSONIO_DIR =  $$PWD/../bsonio/src
BSONIO_GEN_DIR =  ./thrift/gen-cpp
BSONUI_DIR =  $$PWD/../bsonui/src
BSONUI_PLOTS_DIR =    $$PWD/../bsonui/src/charts
BSONUI_HELP_DIR =     $$PWD/../bsonui/src/helpview
BSONUI_SERVICE_DIR =  $$PWD/../bsonui/src/service

DEPENDPATH   += $$SRC_H
DEPENDPATH   += $$BSONIO_GEN_DIR
DEPENDPATH   += $$BSONIO_DIR
DEPENDPATH   += $$BSONUI_DIR
DEPENDPATH   += $$BSONUI_PLOTS_DIR
DEPENDPATH   += $$BSONUI_HELP_DIR
DEPENDPATH   += $$BSONUI_SERVICE_DIR

INCLUDEPATH   += $$SRC_H
INCLUDEPATH   += $$BSONIO_GEN_DIR
INCLUDEPATH   += $$BSONIO_DIR
INCLUDEPATH   += $$BSONUI_DIR
INCLUDEPATH   += $$BSONUI_PLOTS_DIR
INCLUDEPATH   += $$BSONUI_HELP_DIR
INCLUDEPATH   += $$BSONUI_SERVICE_DIR

LIBS += -lyaml-cpp -lejdb -lpugixml
LIBS += -lthrift -lboost_regex -lboost_system -lboost_filesystem
#LIBS += -lapp-static -lhunspell -lmarkdown

MOC_DIR = tmp
UI_DIR        = $$MOC_DIR
UI_SOURSEDIR  = $$MOC_DIR
UI_HEADERDIR  = $$MOC_DIR
OBJECTS_DIR   = obj

include(./ThirdParty/bsonio.pri)
include(./ThirdParty/bsonui.pri)
include($$THRIFT_DIR/thrift.pri)
include($$SRC_CPP/src.pri)

RESOURCES += \
    ../bsonui/bsonui.qrc


#thrift -r -v --gen cpp
