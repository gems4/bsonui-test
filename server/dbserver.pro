TARGET = dbserver
TEMPLATE = app

CONFIG -= qt
CONFIG -= warn_on
CONFIG += warn_off
CONFIG += thread
CONFIG += c++11

!win32 {
  DEFINES += __unix
}

macx-g++ {
  DEFINES += __APPLE__
}

macx-clang {
  DEFINES += __APPLE__
  INCLUDEPATH   += "/usr/local/include"
  DEPENDPATH   += "/usr/local/include"
  LIBPATH += "/usr/local/lib/"
  LIBS += -llua
}
else {

# Define the directory where the gui, third-party libs, resources are located
BUILD_DIR = $$OUT_PWD/..

# Define the directory where the third-party libraries have been installed
#THIRDPARTY_DIR = $$BUILD_DIR/thirdparty/debug
CONFIG(release, debug|release): THIRDPARTY_DIR = $$BUILD_DIR/release/thirdparty
CONFIG(debug, debug|release): THIRDPARTY_DIR = $$BUILD_DIR/debug/thirdparty
# Define the directories where the headers of the third-party libraries have been installed
THIRDPARTY_INCLUDE_DIR = $$THIRDPARTY_DIR/include
# Define the directories where the THIRDPARTY libraries have been installed
THIRDPARTY_LIBRARY_DIR1 = $$THIRDPARTY_DIR/lib

DEPENDPATH   += $$THIRDPARTY_INCLUDE_DIR
INCLUDEPATH   += $$THIRDPARTY_INCLUDE_DIR
LIBS += -L$$THIRDPARTY_LIBRARY_DIR1

  LIBS += -llua5.2
}

# Define the directory where source code is located
THRIFT_DIR    = ../thrift

# Define the directory where bsonio source code is located
BSONIO_DIR =  $$PWD/../../bsonio/src
BSONIO_GEN_DIR =  $$PWD/../../bsonio/src/gen-cpp

DEPENDPATH   += $$BSONIO_GEN_DIR
DEPENDPATH   += $$BSONIO_DIR
INCLUDEPATH   += $$BSONIO_GEN_DIR
INCLUDEPATH   += $$BSONIO_DIR

LIBS += -lyaml-cpp -lejdb -lpugixml
LIBS += -lthrift -lboost_regex


MOC_DIR = tmp
UI_DIR        = $$MOC_DIR
UI_SOURSEDIR  = $$MOC_DIR
UI_HEADERDIR  = $$MOC_DIR
OBJECTS_DIR   = obj

include(../ThirdParty/bsonio.pri)
include($$THRIFT_DIR/thrift.pri)

SOURCES += \
    servermain.cpp

