/**
 * Autogenerated by Thrift Compiler (0.11.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#include "query_types.h"

#include <algorithm>
#include <ostream>

#include <thrift/TToString.h>

namespace query {

int _kQueryStyleValues[] = {
  QueryStyle::EJDB,
  QueryStyle::SPARQL
};
const char* _kQueryStyleNames[] = {
  "EJDB",
  "SPARQL"
};
const std::map<int, const char*> _QueryStyle_VALUES_TO_NAMES(::apache::thrift::TEnumIterator(2, _kQueryStyleValues, _kQueryStyleNames), ::apache::thrift::TEnumIterator(-1, NULL, NULL));

std::ostream& operator<<(std::ostream& out, const QueryStyle::type& val) {
  std::map<int, const char*>::const_iterator it = _QueryStyle_VALUES_TO_NAMES.find(val);
  if (it != _QueryStyle_VALUES_TO_NAMES.end()) {
    out << it->second;
  } else {
    out << static_cast<int>(val);
  }
  return out;
}


Query::~Query() throw() {
}


void Query::__set__id(const std::string& val) {
  this->_id = val;
}

void Query::__set_name(const std::string& val) {
  this->name = val;
}

void Query::__set_comment(const std::string& val) {
  this->comment = val;
}

void Query::__set_style(const QueryStyle::type val) {
  this->style = val;
}

void Query::__set_qschema(const std::string& val) {
  this->qschema = val;
}

void Query::__set_collect(const std::vector<std::string> & val) {
  this->collect = val;
}

void Query::__set_find(const std::string& val) {
  this->find = val;
}
std::ostream& operator<<(std::ostream& out, const Query& obj)
{
  obj.printTo(out);
  return out;
}


uint32_t Query::read(::apache::thrift::protocol::TProtocol* iprot) {

  ::apache::thrift::protocol::TInputRecursionTracker tracker(*iprot);
  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;

  bool isset__id = false;
  bool isset_name = false;
  bool isset_style = false;
  bool isset_collect = false;

  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->_id);
          isset__id = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 2:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->name);
          isset_name = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 3:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->comment);
          this->__isset.comment = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 4:
        if (ftype == ::apache::thrift::protocol::T_I32) {
          int32_t ecast0;
          xfer += iprot->readI32(ecast0);
          this->style = (QueryStyle::type)ecast0;
          isset_style = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 5:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->qschema);
          this->__isset.qschema = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 6:
        if (ftype == ::apache::thrift::protocol::T_LIST) {
          {
            this->collect.clear();
            uint32_t _size1;
            ::apache::thrift::protocol::TType _etype4;
            xfer += iprot->readListBegin(_etype4, _size1);
            this->collect.resize(_size1);
            uint32_t _i5;
            for (_i5 = 0; _i5 < _size1; ++_i5)
            {
              xfer += iprot->readString(this->collect[_i5]);
            }
            xfer += iprot->readListEnd();
          }
          isset_collect = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      case 7:
        if (ftype == ::apache::thrift::protocol::T_STRING) {
          xfer += iprot->readString(this->find);
          this->__isset.find = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  if (!isset__id)
    throw TProtocolException(TProtocolException::INVALID_DATA);
  if (!isset_name)
    throw TProtocolException(TProtocolException::INVALID_DATA);
  if (!isset_style)
    throw TProtocolException(TProtocolException::INVALID_DATA);
  if (!isset_collect)
    throw TProtocolException(TProtocolException::INVALID_DATA);
  return xfer;
}

uint32_t Query::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  ::apache::thrift::protocol::TOutputRecursionTracker tracker(*oprot);
  xfer += oprot->writeStructBegin("Query");

  xfer += oprot->writeFieldBegin("_id", ::apache::thrift::protocol::T_STRING, 1);
  xfer += oprot->writeString(this->_id);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("name", ::apache::thrift::protocol::T_STRING, 2);
  xfer += oprot->writeString(this->name);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("comment", ::apache::thrift::protocol::T_STRING, 3);
  xfer += oprot->writeString(this->comment);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("style", ::apache::thrift::protocol::T_I32, 4);
  xfer += oprot->writeI32((int32_t)this->style);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("qschema", ::apache::thrift::protocol::T_STRING, 5);
  xfer += oprot->writeString(this->qschema);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("collect", ::apache::thrift::protocol::T_LIST, 6);
  {
    xfer += oprot->writeListBegin(::apache::thrift::protocol::T_STRING, static_cast<uint32_t>(this->collect.size()));
    std::vector<std::string> ::const_iterator _iter6;
    for (_iter6 = this->collect.begin(); _iter6 != this->collect.end(); ++_iter6)
    {
      xfer += oprot->writeString((*_iter6));
    }
    xfer += oprot->writeListEnd();
  }
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldBegin("find", ::apache::thrift::protocol::T_STRING, 7);
  xfer += oprot->writeString(this->find);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(Query &a, Query &b) {
  using ::std::swap;
  swap(a._id, b._id);
  swap(a.name, b.name);
  swap(a.comment, b.comment);
  swap(a.style, b.style);
  swap(a.qschema, b.qschema);
  swap(a.collect, b.collect);
  swap(a.find, b.find);
  swap(a.__isset, b.__isset);
}

Query::Query(const Query& other7) {
  _id = other7._id;
  name = other7.name;
  comment = other7.comment;
  style = other7.style;
  qschema = other7.qschema;
  collect = other7.collect;
  find = other7.find;
  __isset = other7.__isset;
}
Query& Query::operator=(const Query& other8) {
  _id = other8._id;
  name = other8.name;
  comment = other8.comment;
  style = other8.style;
  qschema = other8.qschema;
  collect = other8.collect;
  find = other8.find;
  __isset = other8.__isset;
  return *this;
}
void Query::printTo(std::ostream& out) const {
  using ::apache::thrift::to_string;
  out << "Query(";
  out << "_id=" << to_string(_id);
  out << ", " << "name=" << to_string(name);
  out << ", " << "comment=" << to_string(comment);
  out << ", " << "style=" << to_string(style);
  out << ", " << "qschema=" << to_string(qschema);
  out << ", " << "collect=" << to_string(collect);
  out << ", " << "find=" << to_string(find);
  out << ")";
}

} // namespace
