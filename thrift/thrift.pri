THRIFTSOURCES = \
    $$THRIFT_DIR/dbserver.thrift \
    $$THRIFT_DIR/impex.thrift \
    $$THRIFT_DIR/query.thrift


SOURCES += \
    $$BSONIO_GEN_DIR/DBServer.cpp \
    $$BSONIO_GEN_DIR/dbserver_constants.cpp \
    $$BSONIO_GEN_DIR/dbserver_types.cpp \
    $$BSONIO_GEN_DIR/impex_types.cpp \
    $$BSONIO_GEN_DIR/impex_constants.cpp

HEADERS += \
    $$BSONIO_GEN_DIR/DBServer.h \
    $$BSONIO_GEN_DIR/dbserver_constants.h \
    $$BSONIO_GEN_DIR/dbserver_types.h \
    $$BSONIO_GEN_DIR/impex_types.h \
    $$BSONIO_GEN_DIR/impex_constants.h
