# bsonUI-test #

A proof-of-concept code for testing a bsonui framework - read/create -> edit -> save any structured data object in JSON, YAML, XML files and in BSON (embedded) database. This example can work with files (located in Resources/files/) using thrift-generated JSON schemas (located in Resources/data/schemas) and without them.

## Building and Installing on Linux ##

* Make sure that CMake, Qt5, and Qwt6 are installed in your system. For Ubuntu 16.04, this can be done using the following commands:
~~~
sudo apt-get install cmake
sudo apt-get install qt5-default qttools5-dev libqt5svg5 libqt5svg5-dev libqt5help5
sudo apt-get install libqwt-headers libqwt-qt5-6 libqwt-qt5-dev libqwtmathml-qt5-6 libqwtmathml-qt5-dev
~~~

* If not yet done, install Apache Thrift v.0.9.1 (on ubuntu linux) - open the terminal and run the command:
~~~
sudo apt-get install thrift-compiler
~~~

* Alternatively, you can build the actual master version of the Apache Thrift (now v.0.11.0 ) by cloning it with git:
~~~
sudo apt-get install libboost-dev libboost-test-dev libboost-program-options-dev libboost-filesystem-dev 
sudo apt-get libboost-thread-dev libevent-dev automake libtool flex bison pkg-config g++ libssl-dev
cd ~
mkdir thrift
cd thrift
git clone http://github.com/apache/thrift . -b 0.11.0
./bootstrap.sh
./configure
sudo make install
sudo ldconfig
thrift -version
~~~

* If not yet done, install the Lua embedded scripts interpreter (on ubuntu linux):
~~~
sudo apt-get install lua5.2 lua5.2-dev
~~~

* Let us call devBSONUI the directory structure where the bsonUI-test, bsonio and bsonui repositories have to be cloned from their respective git repositories:
~~~
~/devBSONUI
           /BSONUI-test
           /bsonio
           /bsonui
           /build-bsonui-test
                             /debug
                             /release
~~~

* Start QtCreator and configure bsonui-test.pro and server/dbserver.pro projects to build debug and release binaries respectively into
~~~
~/devBSONUI/build-bsonui-test/debug
~/devBSONUI/build-bsonui-test/release
~~~

* Copy the /BSONUI-test/Resources/ sub-folder that contains test data into /build/debug/ (build/release/) folder.

* In a linux terminal, run the following commands to download the bsonio, bsonui libraries, as well as to download, configure, build, and install used third-party libraries:
~~~
cd ~/devBSONUI/bsonio
git clone https://bitbucket.org/gems4/bsonio.git .
cd ~/devBSONUI/bsonui
git clone https://bitbucket.org/gems4/bsonui.git .
cd ~/devBSONUI/BSONUI-test
git clone https://bitbucket.org/gems4/bsonui-test.git .
cd ~/devBSONUI/BSONUI-test
sudo ./install-thirdparty.sh
sudo ldconfig 
~~~

* After this, headers and libraries of the third-party libraries can be found in build/{debug,release}/thirdparty/{include,lib}. The .pro file of the master project has already been adjusted to find these dependencies.

* Now back in QtCreator, build the bsonui-test.pro and server/dbserver.pro projects. 

* Before start the dbserver server configuration file must be defined (see example ~Resources/config/ejdb-server.json)
~~~
{
     "LocalDBDirectory" :   "/home/svd/devPMATCH/build/EJDB",
     "LocalDBName" :   "localdb",
     "host_name" :   "localhost",
     "port" :   9090,
     "protocol" :   "-b",
     "transport" :   "-s"
} 
~~~

* First, start the dbserver executable to start ejdb server for CRUD operation. 
~~~
./dbserver ejdb-server.json
~~~


## Building and Installing on Mac OSX ##

### Prerequisites ###

* Homebrew installed (see [[Homebrew web site][http://brew.sh]] and [[Homebrew on Mac OSX El Capitan][http://digitizor.com/install-homebrew-osx-el-capitan/]]).

* Qt5 (LGPL) with QtCreator for clang (Mac desktop) (see [[Qt5 web site][https://qt.io]]).
 
* Install Apache Thrift - open the terminal and run the commands:
~~~
brew install cmake
brew install thrift
~~~
Copy a file ThirdParty/TBase.h to /usr/local/Cellar/thrift/0.9.3/include/thrift (may need sudo) 

* Install Lua 5.2 embedded scripts interpreter:
~~~
brew install lua
~~~

* Let�s call devBSONUI the directory structure where the bsonUI-test, bsonio and bsonui repositories have to be cloned from their respective git repositories:
~~~
~/devBSONUI
           /BSONUI-test
           /bsonio
           /bsonui
           /build-bsonui-test
                             /debug
                             /release
~~~

* Start QtCreator and configure bsonui-test.pro and ejdb-server.pro projects to build debug and release binaries respectively into
~~~
~/devBSONUI/build-bsonui-test/debug
~/devBSONUI/build-bsonui-test/release
~~~

* In a terminal, run the following commands to clone the bsonio, bsonui and BSONUI-test libraries, as well as to download, configure, build, and install the necessary third-party libraries:
~~~
$ cd ~/devBSONUI/bsonio
$ git clone https://bitbucket.org/gems4/bsonio.git .
$ cd ~/devBSONUI/bsonui
$ git clone https://bitbucket.org/gems4/bsonui.git .
$ cd ~/devBSONUI/BSONUI-test
$ git clone https://bitbucket.org/gems4/bsonui-test.git .
$ cd ~/devBSONUI/BSONUI-test
$ sudo ./install-thirdparty-mac.sh 
~~~

* After this, headers and libraries of the third-party libraries can be found in /usr/local/{include,lib}. The .pro file of the master project has already been adjusted to find these dependencies. 

* Start QtCreator and build the project bsonui-test.pro. Please, do not forget to set the configurator.
