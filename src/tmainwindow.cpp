#include <QFile>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>

#include "tmainwindow.h"
#include "ui_tmainwindow.h"
#include "json2cfg.h"
#include "SelectDialog.h"
#include "PreferencesBSONUI.h"
#include "FormatImportDialog.h"
#include "nejdb.h"
#include "thrift_import.h"
using namespace bsonio;

#ifdef useOldBsonSchema
#include "model_schema.h"
#else
#include "model_node.h"
#endif

void TMainWindow::setDefValues()
{
   // load main programm settingth
   mainSettings = new QSettings("bsonui-test.ini", QSettings::IniFormat);
   getDataFromPreferences();
}

void TMainWindow::getDataFromPreferences()
{
  if( !mainSettings)
   return;
  SchemDir =  mainSettings->value("SchemasDirectory", "").toString();
  UserDir = mainSettings->value("UserFolderPath", ".").toString();
#ifdef useOldBsonSchema
  TBsonSchemaModel::showComments = mainSettings->value("ShowComments",
                            TBsonSchemaModel::showComments).toBool();
  ui->action_Show_comments->setChecked(TBsonSchemaModel::showComments);
  TBsonSchemaModel::useEnumNames = mainSettings->value("UseEnumNames",
                            TBsonSchemaModel::useEnumNames).toBool();
  ui->action_Display_enums->setChecked(TBsonSchemaModel::useEnumNames);
  TBsonSchemaModel::editID = mainSettings->value("Edit_id",
                            TBsonSchemaModel::editID).toBool();
  ui->action_Edit_id->setChecked(TBsonSchemaModel::editID);
#else
  TSchemaNodeModel::showComments = mainSettings->value("ShowComments",
                            TSchemaNodeModel::showComments).toBool();
  ui->action_Show_comments->setChecked(TSchemaNodeModel::showComments);
  TSchemaNodeModel::useEnumNames = mainSettings->value("UseEnumNames",
                            TSchemaNodeModel::useEnumNames).toBool();
  ui->action_Display_enums->setChecked(TSchemaNodeModel::useEnumNames);
  TSchemaNodeModel::editID = mainSettings->value("Edit_id",
                            TSchemaNodeModel::editID).toBool();
  ui->action_Edit_id->setChecked(TSchemaNodeModel::editID);
#endif

  LocalDBDir = mainSettings->value("LocalDBPath", "./EJDB/localejdb").toString();
  useLocalDB = mainSettings->value("UseLocalDB", false).toBool();
  if( !LocalDBDir.isEmpty() )
    flEJ.reOpen(LocalDBDir.toUtf8().data());
}


TMainWindow::TMainWindow(QWidget *parent) :
    QMainWindow(parent), useLocalDB(false),
    ui(new Ui::TMainWindow)
{
    model_free = 0;
    deleg_free = 0;
    model_schema = 0;
    deleg_schema = 0;
    fieldTable = 0;

   ui->setupUi(this);
   //set up main parameters
   setDefValues();
   bson_init(&curRecord);
   bson_finish(&curRecord);

   // showBson();
   aHeaderData << "key" << "value" << "comment" ;
   fieldTable =  new TBsonView(  ui->centralWidget );
   deleg_free =  new TBsonDelegate( ui->centralWidget);

#ifdef useOldBsonSchema
   deleg_schema = new TBsonSchemaDelegate( ui->centralWidget);
#else
   deleg_schema = new TSchemaNodeDelegate( ui->centralWidget);
#endif
   //fieldTable->setItemDelegate(deleg);
   //fieldTable->setModel(model);
   fieldTable->setColumnWidth( 0, 250 );
   //fieldTable->expandToDepth(5);
   ui->gridLayout->addWidget(fieldTable, 1, 0, 1, 2);

   setActions();
   CmNew();
   readSchemaDir( SchemDir );
   SchemaNode::_schema = &schema;
}

TMainWindow::~TMainWindow()
{
    delete ui;
}

//  Connect all actions
void TMainWindow::setActions()
{
    // File
    connect( ui->actionTest_BSONUIWidget, SIGNAL( triggered()), this, SLOT(CmTestBsonui()));

    connect( ui->actionAdd_schema_file, SIGNAL( triggered()), this, SLOT(CmReadSchema()));
    connect( ui->actionNew , SIGNAL( triggered()), this, SLOT(CmNew()));
    connect( ui->actionE_xit, SIGNAL( triggered()), qApp/*this*/, SLOT(quit()/*close()*/));
    connect( ui->actionExport_Json_File, SIGNAL( triggered()), this, SLOT(CmExportJSON()));
    connect( ui->actionImport_Json_File, SIGNAL( triggered()), this, SLOT(CmImportJSON()));
    connect( ui->action_Export_YAML_File, SIGNAL( triggered()), this, SLOT(CmExportYAML()));
    connect( ui->actionImport_YAML_File, SIGNAL( triggered()), this, SLOT(CmImportYAML()));
    connect( ui->actionExport_XML_File, SIGNAL( triggered()), this, SLOT(CmExportXML()));
    connect( ui->actionImport_XML_File, SIGNAL( triggered()), this, SLOT(CmImportXML()));
    connect( ui->actionExport_Arbitrary_Format_File, SIGNAL( triggered()), this, SLOT(CmExportArbitrary()));
    connect( ui->actionImport_Arbitrary_Format_File, SIGNAL( triggered()), this, SLOT(CmImportArbitrary()));

    // Edit
    connect(ui->action_Add_fields, SIGNAL(triggered()), fieldTable, SLOT(CmAddObjects()));
    connect(ui->actionAdd_one_field, SIGNAL(triggered()), fieldTable, SLOT(CmAddObject()));
    connect(ui->action_Delete_field, SIGNAL(triggered()), fieldTable, SLOT(CmDelObject()));
    connect(ui->actionRemove_Alternatives_Union, SIGNAL(triggered()), fieldTable, SLOT(CmDelObjectsUnion()));
    connect(ui->action_Resize_array, SIGNAL(triggered()), fieldTable, SLOT(CmResizeArray()));
    connect(ui->action_Calculator, SIGNAL(triggered()), fieldTable, SLOT(CmCalc()));
    connect(ui->actionSelect_row, SIGNAL(triggered()), fieldTable, SLOT(SelectRow()));
    connect(ui->actionSelect_co_lumn, SIGNAL(triggered()), fieldTable, SLOT(SelectColumn()));
    connect(ui->actionSelect_group, SIGNAL(triggered()), fieldTable, SLOT(SelectGroup()));
    connect(ui->actionSelect_all, SIGNAL(triggered()), fieldTable, SLOT(SelectAll()));
    connect(ui->action_Copy, SIGNAL(triggered()), fieldTable, SLOT(CopyData()));
    connect(ui->actionCopy_with_names, SIGNAL(triggered()), fieldTable, SLOT(CopyDataHeader()));

    // Help
    connect( ui->action_Help_About, SIGNAL( triggered()), this, SLOT(CmHelpAbout()));
    connect( ui->actionContents, SIGNAL( triggered()), this, SLOT(CmHelpContens()));
    connect( ui->actionAuthors, SIGNAL( triggered()), this, SLOT(CmHelpAuthors()));
    connect( ui->actionLicense, SIGNAL( triggered()), this, SLOT(CmHelpLicense()));
    connect( ui->actionPreferences, SIGNAL( triggered()), this, SLOT(CmSettingth()));

    // View
    connect( ui->action_Show_comments, SIGNAL( changed()), this, SLOT(CmShowComments()));
    connect( ui->action_Display_enums, SIGNAL( triggered()), this, SLOT(CmDisplayEnums()));
    connect( ui->action_Edit_id, SIGNAL( triggered()), this, SLOT(CmEditID()));

// Record
    connect( ui->action_Update, SIGNAL( triggered()), this, SLOT(CmUpdate()));
    connect( ui->action_Create, SIGNAL( triggered()), this, SLOT(CmCreateInsert()));
    connect( ui->action_Delete, SIGNAL( triggered()), this, SLOT(CmDelete()));
    connect( ui->action_Read, SIGNAL( triggered()), this, SLOT(CmRead()));
    connect( ui->actionImport, SIGNAL( triggered()), this, SLOT(CmImportFormat()));

   //pLineTask = new QLineEdit( ui->toolBarTask );
   //pLineTask->setEnabled( true );
   //pLineTask->setFocusPolicy( Qt::ClickFocus );
   //pLineTask->setReadOnly( true );
   //pLineTask->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
   //ui->toolBarTask->setSizePolicy( QSizePolicy::Expanding, QSizePolicy::Fixed );
   //ui->toolBarTask->addWidget( pLineTask ); // setStretchableWidget( pLine );
}

void TMainWindow::CmSettingth()
{
  try
  {
     QString oldSchemaDir = SchemDir;
     QString oldLocalDBDir = LocalDBDir;
     bool oldUseLoacaDB = useLocalDB;

     // define new preferences
     PreferencesBSONUI dlg(mainSettings);
      if( !dlg.exec() )
          return;
    //get data from settings
    getDataFromPreferences();

    if( oldUseLoacaDB != useLocalDB ||
      ( useLocalDB && oldLocalDBDir != LocalDBDir))
    { // changed DB location
      resetDBClient(curSchemaName );
    }

    if( oldSchemaDir != SchemDir )
    {
      // clear all
      readSchemaDir( SchemDir );
      if( !curSchemaName.empty() )
        CmNew();
    }
    else
    {
        // reset model
        if( !curSchemaName.empty() )
        {   model_schema->updateModelData();
            fieldTable->header()->resizeSection(0, 250);
            fieldTable->header()->resizeSection(1, 250);
            //     resetBson( curSchemaName );
        }
     }

  }
  catch(std::exception& e)
  {
        QMessageBox::critical( this, "CmSettingth", e.what() );
  }
}

void TMainWindow::CmShowComments()
{
#ifdef useOldBsonSchema
  TBsonSchemaModel::showComments = ui->action_Show_comments->isChecked();
  mainSettings->setValue("ShowComments",  TBsonSchemaModel::showComments );
#else
    TSchemaNodeModel::showComments = ui->action_Show_comments->isChecked();
    mainSettings->setValue("ShowComments",  TSchemaNodeModel::showComments );
#endif

  mainSettings->sync();
  if( !curSchemaName.empty() )
  {   model_schema->updateModelData();
      fieldTable->header()->resizeSection(0, 250);
      fieldTable->header()->resizeSection(1, 250);
      //     resetBson( curSchemaName );
  }
}

void TMainWindow::CmDisplayEnums()
{
#ifdef useOldBsonSchema
    TBsonSchemaModel::useEnumNames = ui->action_Display_enums->isChecked();
    mainSettings->setValue("UseEnumNames",  TBsonSchemaModel::useEnumNames );
#else
    TSchemaNodeModel::useEnumNames = ui->action_Display_enums->isChecked();
    mainSettings->setValue("UseEnumNames",  TSchemaNodeModel::useEnumNames );
#endif
  mainSettings->sync();
  fieldTable->hide();
  fieldTable->show();
}

void TMainWindow::CmEditID()
{
#ifdef useOldBsonSchema
    TBsonSchemaModel::editID = ui->action_Edit_id->isChecked();
    mainSettings->setValue("Edit_id",  TBsonSchemaModel::editID );
#else
    TSchemaNodeModel::editID = ui->action_Edit_id->isChecked();
    mainSettings->setValue("Edit_id",  TSchemaNodeModel::editID );
#endif
  mainSettings->sync();
}

/// Read thrift schemas from json files
void TMainWindow::CmReadSchema()
{
  try{
       QString dirPath = QFileDialog::getExistingDirectory(this, "Select Schemas Directory",
         SchemDir,  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );
        if( dirPath.isEmpty() )
          return;

        readSchemaDir( dirPath );
        SchemDir =  dirPath;
        mainSettings->setValue( "SchemasDirectory", SchemDir );
        mainSettings->sync();
    }
    catch(bsonio_exeption& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( this, "std::exception", e.what() );
     }
}

/// Read bson record from json file fileName
void TMainWindow::CmNew()
{
  try{
          bson_destroy( &curRecord );
          bson_init( &curRecord );
          bson_finish( &curRecord );
          resetBson("");
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Save current record to DB file as new
void TMainWindow::CmCreateInsert()
{
    try
    {
        if( dbclient.get() == 0 )
          return;
        string recBsonText;
        jsonFromBson( curRecord.data, recBsonText );
        dbclient->SetJsonYaml( recBsonText,  true );
        dbclient->InsertRecord();
        // contentsChanged = false;
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read new record from DB
void TMainWindow::CmRead()
{
    try
    {
        if( dbclient.get() == 0 )
          return;

        // Select keys to load
        vector<string> aKeys;
        dbclient->GetKeyList( "*", aKeys, true );
        if( aKeys.empty() )
          return;

        SelectDialog selDlg( this, "Please, select record to read", aKeys, 0, ':' );
         if( !selDlg.exec() )
          return;

        string reckey = aKeys[selDlg.selIndex()];
        // Read Record
        dbclient->Get( reckey.c_str() );
        string valDB = dbclient->GetJson();
        bson_destroy( &curRecord );
        jsonToBson( &curRecord, valDB );
        resetBson( dbclient->getKeywd() );
        // contentsChanged = false;
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Save current record to DB file
void TMainWindow::CmUpdate()
{
    try
    {
       if( dbclient.get() == 0 )
          return;
        string recBsonText;
        jsonFromBson( curRecord.data, recBsonText );
        dbclient->SetJsonYaml( recBsonText,  true );
        // get current key
        string key = dbclient->getKeyFromBson( curRecord.data );
        dbclient->SaveRecord( key.c_str() );
        // contentsChanged = false;
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Delete current record from DB
void TMainWindow::CmDelete()
{
    try
    {
        if( dbclient.get() == 0 )
          return;
        // get current key
        string key = dbclient->getKeyFromBson( curRecord.data );
        dbclient->Del( key.c_str() );
        // contentsChanged = false;
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Read bson record from json file fileName
void TMainWindow::CmImportJSON()
{
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select file with json object", "*.json"  ))
       {
          FJson file( fileName);
          file.setType( FileTypes::Json_ );
          bson_destroy( &curRecord );
          file.LoadBson( &curRecord );
          resetBson(schemafromName( fileName ));
       }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to json file fileName
void TMainWindow::CmExportJSON()
{
   try {
         string fileName;
         const char* schemaext = 0;
         string mainext = fileShemaExt("json");
         if( !mainext.empty() )
         {   schemaext = mainext.c_str();
             fileName = schemaext;
         }
         if(  ChooseFileSave( this, fileName,
                     "Please, select file to write", "*.json", schemaext  ))
         {
            FJson file( fileName);
            file.setType( FileTypes::Json_ );
            file.SaveBson( curRecord.data );
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


/// Read bson record from yaml file fileName
void TMainWindow::CmImportYAML()
{
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select file with YAML object", "*.yaml"  ))
       {
          FJson file( fileName);
          file.setType( FileTypes::Yaml_ );
          bson_destroy( &curRecord );
          file.LoadBson( &curRecord );
          resetBson(schemafromName( fileName ));
       }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to YAML file fileName
void TMainWindow::CmExportYAML()
{
   try {
        string fileName;
        const char* schemaext = 0;
        string mainext = fileShemaExt("yaml");
        if( !mainext.empty() )
        {   schemaext = mainext.c_str();
            fileName = schemaext;
        }
        if(  ChooseFileSave( this, fileName,
                     "Please, select file to write", "*.yaml", schemaext  ))
         {
            FJson file( fileName);
            file.setType( FileTypes::Yaml_ );
            file.SaveBson( curRecord.data );
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Read bson record from xml file fileName
void TMainWindow::CmImportXML()
{
  try{
        string fileName;
        if(  ChooseFileOpen( this, fileName,
                     "Please, select file with xml object", "*.xml"  ))
       {
          FJson file( fileName);
          file.setType( FileTypes::XML_ );
          bson_destroy( &curRecord );
          file.LoadBson( &curRecord );
          resetBson(schemafromName( fileName ));
       }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Write bson record to file fileName
void TMainWindow::CmExportXML()
{
   try {
        string fileName;
        const char* schemaext = 0;
        string mainext = fileShemaExt("xml");
        if( !mainext.empty() )
        {   schemaext = mainext.c_str();
            fileName = schemaext;
        }
        if(  ChooseFileSave( this, fileName,
                     "Please, select file to write", "*.xml", schemaext  ))
         {
            FJson file( fileName);
            file.setType( FileTypes::XML_ );
            file.SaveBson( curRecord.data );
         }
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Read bson records from format file fileName
void TMainWindow::CmImportFormat()
{
    AbstractIEFile *inputIEFile = 0;
    try{
        // Select format&input file
        FormatImportDialog dlg(mainSettings);
         if( !dlg.exec() )
             return;

       int frmtType = mainSettings->value("ImportFileFormat", "3").toInt();
       string formatFileName = mainSettings->value("FormatFilePath", "").toString().toUtf8().data();
       string inputFileName = mainSettings->value("InputFilePath", ".").toString().toUtf8().data();
       bool overwrite = mainSettings->value("ImportFormatOverwrite", false).toBool();

       // Read format to bson
       bson formatBson;
       FJson file( formatFileName); // type from extension
       file.LoadBson( &formatBson );

       // Read format to structure
       switch( frmtType)
       {
         case 0:
            break;
         case 1:
          break;
         case 2:
          break;
         case 3:
          {
            FormatStructDataFile fformatdata;
            readDataFromBsonSchema( &schema, &formatBson, "FormatStructDataFile",  &fformatdata );
            //Create input structure
            inputIEFile = new  StructDataIEFile( &schema, fformatdata );
          }
          break;
        default:
          return;

       }

       if( !inputIEFile )
         return; // only for defined types

        // select main schema
        bson_destroy( &curRecord );
        bson_init( &curRecord );
        bson_finish( &curRecord );
        resetBson(inputIEFile->getDataName());
        // test access to data base to save
        if( dbclient.get() == 0 )
           return;

       // open file with imported data
        inputIEFile->openFile( inputFileName );
        while( inputIEFile->readBlock( &curRecord) )
        {
          string recBsonText;
          string key = dbclient->getKeyFromBson( curRecord.data );
          if( overwrite || !dbclient->Find( key.c_str() ))
          {
             jsonFromBson( curRecord.data, recBsonText );
             dbclient->SetJsonYaml( recBsonText,  true );
             dbclient->SaveRecord( key.c_str() );
          }
        }
        inputIEFile->closeFile();

       delete inputIEFile;
    }
   catch(bsonio_exeption& e)
   {
       delete inputIEFile;
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
        delete inputIEFile;
       QMessageBox::critical( this, "std::exception", e.what() );
    }

}

/// Write bson record to format file fileName
void TMainWindow::CmExportFormat()
{
   try {
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


//-----------------------------------------------------

/// Read all thrift schemas from Dir
void TMainWindow::readSchemaDir( const QString& dirPath )
{
  if( dirPath.isEmpty() )
          return;

  QDir dir(dirPath);
  QStringList nameFilter;
  nameFilter << "*.schema.json";
  QFileInfoList files = dir.entryInfoList( nameFilter, QDir::Files );

  schema.clearAll();
  CmNew();
  string fName;
  foreach (QFileInfo file, files)
  {
   //   fName = file.absoluteFilePath().toUtf8().data();
   fName = file.filePath().toUtf8().data();
   //cout << "FILE: " << fName << endl;
   schema.addSchemaFile( fName.c_str() );
 }

}



void TMainWindow::resetDBClient(const string& schemaName_ )
{
  // no schema
  if( schemaName_.empty() )
  {
      dbclient.reset();
      return;
  }
  // the same schema
  if( dbclient.get() !=  0 && dbclient.get()->getKeywd() == schemaName_ )
    return;

  ThriftStructDef* strDef = schema.getStruct(schemaName_);
  if( strDef == nullptr )
      dbclient.reset();
  else
  {
    vector<KeyFldsData> keyFldsInf;
    uint ii;
    ThriftFieldDef*  fldDef;

   if( strDef->getSelectSize() > 0 )
    {
        string fldKey;
        string to_select_name;
        string to_select_doc;

        for( ii=0; ii<strDef->getSelectSize(); ii++ )
        {
         fldKey = strDef->keyFldIdName(ii);
         schema.getSelectData(fldKey, strDef, to_select_name, to_select_doc );
         keyFldsInf.push_back( KeyFldsData( to_select_name, fldKey, to_select_doc ));
        }

    }
    else // old type of keys
      for( ii=0; ii<strDef->fields.size(); ii++ )
      {
       fldDef = &strDef->fields[ii];
       if( fldDef->fRequired == fld_required )
         keyFldsInf.push_back( KeyFldsData( fldDef->fName, fldDef->fName, fldDef->fDoc ));
      }

    if( useLocalDB )
    {
        TEJDBString* newClient =  new TEJDBString( schemaName_.c_str(), keyFldsInf  );
        dbclient.reset( newClient );
    }
    else
    {  TClientDBString* newClient =  new TClientDBString( schemaName_.c_str(), keyFldsInf  );
       dbclient.reset( newClient );
    }

    try{
         dbclient->Open();
       }
    catch(bsonio_exeption& e)
    {
        cout << "Internal comment " << e.title() << e.what() << endl;
        // QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( this, "std::exception", e.what() );
     }

    dbclient->setKey( "*" );
  }
}

void TMainWindow::resetBson(const string& schemaName )
{
   ChooseSchema( schemaName );

   if( curSchemaName.empty() )
   {
     if( !model_free )
      model_free = new TBsonModel(  &curRecord, aHeaderData, this/*ui->centralWidget*/ );
     else
      model_free->setupModelData(&curRecord, "" );
     fieldTable->setModel(model_free);
     fieldTable->setItemDelegate(deleg_free);
     fieldTable->update();
   }
   else
   {
     if( !model_schema )
#ifdef useOldBsonSchema
         model_schema = new TBsonSchemaModel(  &schema, &curRecord,
          curSchemaName, aHeaderData, this/*ui->centralWidget*/ );
#else
         model_schema = new TSchemaNodeModel(  &schema, &curRecord,
          curSchemaName, aHeaderData, this/*ui->centralWidget*/ );
#endif
     else
      model_schema->setupModelData(&curRecord, curSchemaName );
     fieldTable->setModel(model_schema);
     fieldTable->setItemDelegate(deleg_schema);
     fieldTable->header()->resizeSection(0, 250);
     fieldTable->header()->resizeSection(1, 250);
     fieldTable->update();
   }
}

void TMainWindow::ChooseSchema( const string& schemaName )
{
   curSchemaName = "";

   if( !schemaName.empty() && schema.getStruct(schemaName) != nullptr )
      curSchemaName = schemaName;
   else
   {    // try to select schema
#ifdef useOldBsonSchema
       vector<string> sch_lst = schema.getStructsList( TBsonSchemaModel::showComments );
#else
       vector<string> sch_lst = schema.getStructsList( TSchemaNodeModel::showComments );
#endif
       // add comment
      if( !sch_lst.empty() )
      { SelectDialog selDlg( this, "Please, select schema for bson data", sch_lst, 0, ',' );
         if( selDlg.exec() )
             curSchemaName = sch_lst[selDlg.selIndex()].substr( 0,
                     sch_lst[selDlg.selIndex()].find_first_of(","));

       }
   }
   ui->schemaName->setText(curSchemaName.c_str());
   resetDBClient(curSchemaName );
}

string TMainWindow::fileShemaExt( const char* ext )
{
  string _ret = "";
  if( !curSchemaName.empty())
  {
      _ret += "*.";
      _ret += curSchemaName;
      _ret += ".";//".schema.";
      _ret += ext;
  }
  return _ret;
}

string TMainWindow::schemafromName( const string& fname )
{
   string _ret = "";
   size_t pose = fname.find_last_of(".");
   if( pose != std::string::npos)
   {
      size_t posb = fname.find_last_of(".", pose-1 );
      if( posb != std::string::npos )
        _ret = fname.substr(posb+1, pose-posb-1 );
   }
  return _ret;
}

#include "BSONUIWidget.h"

/// Read bson record from json file fileName
void TMainWindow::CmTestBsonui()
{
  try{
      string testschema ="";
#ifdef useOldBsonSchema
       vector<string> sch_lst = schema.getStructsList( TBsonSchemaModel::showComments );
#else
       vector<string> sch_lst = schema.getStructsList( TSchemaNodeModel::showComments );
#endif
       // add comment
      if( !sch_lst.empty() )
      { SelectDialog selDlg( this, "Please, select schema for bson data", sch_lst, 0, ',' );
         if( selDlg.exec() )
             testschema = sch_lst[selDlg.selIndex()].substr( 0,
                     sch_lst[selDlg.selIndex()].find_first_of(","));

      }
      BSONUIWidget* testWidget = new BSONUIWidget( mainSettings, &schema,testschema, this);
      testWidget->show();

    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/*
bool TMainWindow::ChooseFileOpen(QWidget* par, string& path_,
   const char* title, const char *filter )
{
    QString path;
    if( path_.find('/') == string::npos )
           path  =  UserDir+"/"+path_.c_str();
         else
           path = path_.c_str();

    QString filt;
    if( filter )
        filt = QString("Text files (%1);;All files (*)").arg(filter);
    else
        filt = "All files (*)";

    QString fn = QFileDialog::getOpenFileName(  par, title,
          path, filt, 0, QFileDialog::DontConfirmOverwrite );
#ifdef buildWIN32
    std::replace( fn.begin(), fn.end(), '/', '\\');
#endif
   if ( !fn.isEmpty() )
    {
       QFileInfo flinfo(fn);
       UserDir = flinfo.dir().path();
       path_ = fn.toUtf8().data();
       return true;
    }
    else
    {
        path_ = "";
        return false;
    }

}
*/
/*bool TMainWindow::ChooseFileSave(QWidget* par, string& path_,
       const char* title, const char *filter,  const char *schemafilter )
{
     QString path;
     if( path_.find('/') == string::npos )
       path  =  UserDir+"/"+path_.c_str();
     else
       path = path_.c_str();

    //replace(path.begin(), path.end(),'\\', '/');

    QStringList filt;
    if( schemafilter )
        filt += QString("Schema (%1)").arg(schemafilter);
    if( filter )
        filt += QString("Files (%1)").arg(filter);
    else
        filt += "Text files (*.txt)";

    filt += "All files (*.*)";

    QString selectedFilter;
    QString fn = QFileDialog::getSaveFileName( par, title,
         path, filt.join( ";;" ), &selectedFilter,
         QFileDialog::DontConfirmOverwrite);

    if ( !fn.isEmpty() )
    {
        QFileInfo flinfo(fn);
        UserDir = flinfo.dir().path();
        if( flinfo.suffix().isEmpty() ) // solwing for linux
        {
          int posb = selectedFilter.lastIndexOf(".");
          if( schemafilter )
             posb = selectedFilter.indexOf(".");
          int pose = selectedFilter.indexOf(")", posb);
          if( posb > 0 && pose > posb )
          { QString ext = selectedFilter.mid(posb+1, pose-posb-1);
            fn += "."+ext;
          }
        }
        path_ = fn.toUtf8().data();
        return true;
    }
    else
    {
        path_  = "";
        return false;
    }
}
*/
