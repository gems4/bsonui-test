#ifndef TMAINWINDOW_H
#define TMAINWINDOW_H

#include <string>
#include <QMainWindow>
#include <QSettings>
using namespace std;

#include "thrift_schema.h"
#include "v_json.h"
#include "model_bson.h"
#include "dbclient.h"

namespace Ui {
class TMainWindow;
}

class TMainWindow : public QMainWindow
{
    Q_OBJECT

    // Internal settings
    QSettings *mainSettings;        ///< Properties for program
    QString SchemDir;               ///< Path to schemas directory
//    QString UserDir;                ///< Path to User directory
    QString LocalDBDir;             ///< Path to Local EJDB
    bool useLocalDB;                ///< Flag for using local DB (no server )

    // Internal data
    ThriftSchema schema;
    string curSchemaName = "";
    bson curRecord;
//    boost::shared_ptr<bsonio::TClientDBString> dbclient;
    boost::shared_ptr<bsonio::TDBAbstract> dbclient;

    vector<unique_ptr<QMainWindow>> testWindows;

    // Work functions
    void setDefValues();
    void getDataFromPreferences();
    void setActions();
    void readSchemaDir( const QString& dirPath );
    void resetBson(const string& schemaName );
    void resetDBClient(const string& schemaName );
    string fileShemaExt(const char* ext);
    string schemafromName( const string& fname );



     // Internal dialogs
//    bool ChooseFileOpen(QWidget* par, string& path_,
//              const char* title, const char *filter );
//    bool ChooseFileSave(QWidget* par, string& path_,
//           const char* title, const char *filter,
//                        const char *mainfilter = 0 );
    void ChooseSchema( const string& schemaName );


public slots:
    void objectChanged() {}

    // Help
    void CmHelpAbout(){}
    void CmHelpAuthors(){}
    void CmHelpLicense(){}
    void CmHelpContens(){}
    void CmSettingth();

    // View
    void CmShowComments();
    void CmDisplayEnums();
    void CmEditID();

   // Record
    void CmCreateInsert();
    void CmRead();
    void CmUpdate();
    void CmDelete();
    void CmImportFormat();
    void CmExportFormat();

    // File
    void CmReadSchema();
    void CmNew();
    void CmTestBsonui();
    void CmExportJSON();
    void CmImportJSON();
    void CmExportYAML();
    void CmImportYAML();
    void CmExportXML();
    void CmImportXML();
    void CmExportArbitrary(){}
    void CmImportArbitrary(){}

public:
    explicit TMainWindow(QWidget *parent = 0);
    ~TMainWindow();

private:
    Ui::TMainWindow *ui;

    QStringList aHeaderData;
    TBsonAbstractModel* model_free;
    QItemDelegate *deleg_free;
    TBsonAbstractModel* model_schema;
    QItemDelegate *deleg_schema;
    TBsonView* fieldTable;

};

#endif // TMAINWINDOW_H
