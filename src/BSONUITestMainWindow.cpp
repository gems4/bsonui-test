#include <functional>
#include <QFile>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>

#include "BSONUITestMainWindow.h"
#include "ui_BSONUITestMainWindow.h"
#include "BSONUIWidget.h"
#include "VertexWidget.h"
#include "EdgesWidget.h"
#include "FormatImpexWidget.h"
#include "TableEditWindow.h"
#include "HelpMainWindow.h"
#include "SelectDialog.h"
#include "preferences.h"
using namespace bsonio;
using namespace bsonui;

TBSONUITestWin::TBSONUITestWin(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TBSONUITestWin)
{
   ui->setupUi(this);

   onCloseEvent = [=](QMainWindow* win)
   {
       auto it = bsonuiWindows.begin();
       while( it != bsonuiWindows.end() )
        if( *it == win ) // pointer
        {  bsonuiWindows.erase(it);
           break;
        } else
            it++;
       return true;
   };

   showWidget =  [=]( bool isVertex, const string& testschema,
           const string& recordkey, const string& queryString )
           {
               OpenNewWidget( isVertex, testschema, recordkey, queryString );
           };

   //set up main parameters
   setActions();

   // init help window
   new HelpMainWindow( 0 );
}

TBSONUITestWin::~TBSONUITestWin()
{
    delete _csvWin;
    delete ui;
}

//  Connect all actions
void TBSONUITestWin::setActions()
{
    // File
    connect( ui->actionSet_I_O_Files_Location, SIGNAL( triggered()), this, SLOT(CmSetWorkDir()));
    connect( ui->actionNewVertex , SIGNAL( triggered()), this, SLOT(CmNewVertex()));
    connect( ui->actionNew_Edge_Editor_Window , SIGNAL( triggered()), this, SLOT(CmNewEdge()));
    connect( ui->actionNew_Bson , SIGNAL( triggered()), this, SLOT(CmNewBson()));
    connect( ui->actionNew_CSV , SIGNAL( triggered()), this, SLOT(CmNewCsv()));
    connect( ui->actionNew_Impex , SIGNAL( triggered()), this, SLOT(CmNewImpexFormat()));
//    connect( ui->actionE_xit, SIGNAL( triggered()), this, SLOT(close()));
    connect( ui->actionE_xit, SIGNAL( triggered()), qApp, SLOT(quit()));

     // Help
    connect( ui->action_Help_About, SIGNAL( triggered()), this, SLOT(CmHelpAbout()));
    connect( ui->actionContents, SIGNAL( triggered()), this, SLOT(CmHelpContens()));
    connect( ui->actionAuthors, SIGNAL( triggered()), this, SLOT(CmHelpAuthors()));
    connect( ui->actionLicense, SIGNAL( triggered()), this, SLOT(CmHelpLicense()));
    connect( ui->actionPreferences, SIGNAL( triggered()), &uiSettings(), SLOT(CmSettingth()));

    // View
    connect( ui->action_Show_comments, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmShowComments(bool)));
    connect( ui->action_Display_enums, SIGNAL( toggled(bool)), &uiSettings(), SLOT(CmDisplayEnums(bool)));
    connect( ui->action_Edit_id, SIGNAL(toggled(bool)), &uiSettings(), SLOT(CmEditID(bool)));

    /// connect( &uiSettings(), SIGNAL(dbChanged()), this, SLOT(closeAll()));
    connect( &uiSettings(), SIGNAL(dbChanged()), this, SLOT(updateDB()));
    connect( &uiSettings(), SIGNAL(schemaChanged()), this, SLOT(closeAll()));
    connect( &uiSettings(), SIGNAL(viewMenuChanged()), this, SLOT(updateViewMenu()));
    connect( &uiSettings(), SIGNAL(modelChanged()), this, SLOT(updateModel()));
    connect( &uiSettings(), SIGNAL(tableChanged()), this, SLOT(updateTable()));
 //   connect( &uiSettings(), SIGNAL(errorSettings(string)), this, SLOT(CmEditID(bool)));

}

void TBSONUITestWin::CmHelpContens()
{
  helpWin( "bsonui-test", "" );
}



/// Define new input/output directory
void TBSONUITestWin::CmSetWorkDir()
{
  try{
       QString dirPath = QFileDialog::getExistingDirectory(this, "Select I/O Files Location",
         ioSettings().userDir().c_str(),  QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks );
        if( dirPath.isEmpty() )
          return;
        ioSettings().setUserDir(dirPath.toStdString());
    }
    catch(bsonio_exeption& e)
    {
        QMessageBox::critical( this, e.title(), e.what() );
    }
    catch(std::exception& e)
     {
        QMessageBox::critical( this, "std::exception", e.what() );
     }
}


/// Open new EdgesWidget  or VertexWidget  window
void TBSONUITestWin::OpenNewWidget( bool isVertex, const string& testschema,
                        const string& recordkey, const string& queryString )
{
      BSONUIBase* testWidget;
      if( isVertex )
      {
          testWidget = new VertexWidget( testschema/*, this*/ );
          connect( (VertexWidget*)testWidget, SIGNAL(vertexDeleted()), this, SLOT(onDeleteVertex()));
      }
      else
      {
          testWidget = new EdgesWidget( testschema, queryString/*, this*/ );
          connect( (EdgesWidget*)testWidget, SIGNAL(edgeDeleted()), this, SLOT(onDeleteEdge()));
      }
      testWidget->setOnCloseEventFunction(onCloseEvent);
      testWidget->setShowWidgetFunction(showWidget);
      bsonuiWindows.push_back(testWidget);

      if( !recordkey.empty() )
      {    testWidget->openRecordKey(  recordkey  );
           if( isVertex )
              testWidget->changeKeyList();
      }
      testWidget->show();
}

/// Open new BSONUIWidget window
void TBSONUITestWin::CmNewBson()
{
  try{
      string testschema ="";

      vector<string> sch_lst = ioSettings().Schema()->getStructsList( TSchemaNodeModel::showComments );
       // add comment
      if( !sch_lst.empty() )
      { bsonui::SelectDialog selDlg( this, "Please, select schema to edit data", sch_lst, 0, ',' );
         if( selDlg.exec() )
             testschema = sch_lst[selDlg.selIndex()].substr( 0,
                     sch_lst[selDlg.selIndex()].find_first_of(","));

      }

      BSONUIBase* testWidget = new BSONUIWidget(  testschema/*, this*/ );

      testWidget->setOnCloseEventFunction(onCloseEvent);
      testWidget->setShowWidgetFunction(showWidget);
      bsonuiWindows.push_back(testWidget);
      testWidget->show();
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Open new BSONUIWidget with ImpexFormat&database window
void TBSONUITestWin::CmNewImpexFormat()
{
  try{

      BSONUIBase* testWidget = new FormatImpexWidget(
                      FormatImpexWidget::editMode /*, "", this*/ );

      testWidget->setOnCloseEventFunction(onCloseEvent);
      testWidget->setShowWidgetFunction(showWidget);
      bsonuiWindows.push_back(testWidget);
      testWidget->show();
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Open new VertexWidget window
void TBSONUITestWin::CmNewVertex()
{
  try{
      string testschema ="";

      //vector<string> sch_lst = schema.getStructsList( TSchemaNodeModel::showComments );
      vector<string> sch_lst = ioSettings().Schema()->getVertexesList();
       // add comment
      if( !sch_lst.empty() )
      { bsonui::SelectDialog selDlg( this, "Please, select vertex schema to edit data", sch_lst, 0, ',' );
         if( selDlg.exec() )
             testschema = sch_lst[selDlg.selIndex()].substr( 0,
                     sch_lst[selDlg.selIndex()].find_first_of(","));

      }
      OpenNewWidget( true, testschema, "" );
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Open new EdgesWidget window
void TBSONUITestWin::CmNewEdge()
{
  try{
      OpenNewWidget( false, "", "" );
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}

/// Open new CSVEditWidget window
void TBSONUITestWin::CmNewCsv()
{
  try{

        string fileName;
        if(  !ChooseFileOpen( this, fileName,
                "Please, select file with csv object", "*.csv"  ))
           return;
        // define new dialog
        if(!_csvWin)
        {
            _csvWin = new TableEditWidget("CSV editor ", fileName,
              TMatrixTable::tbEdit|TMatrixTable::tbGraph/*|TMatrixTable::tbSort*/ );
        }
        else
        {
           _csvWin->openNewCSV(fileName);
           _csvWin->raise();
        }
        _csvWin->show();
    }
   catch(bsonio_exeption& e)
   {
       QMessageBox::critical( this, e.title(), e.what() );
   }
   catch(std::exception& e)
    {
       QMessageBox::critical( this, "std::exception", e.what() );
    }
}


//-----------------------------------------------------


/// Update menu for all windows
void TBSONUITestWin::updateViewMenu()
{
    ui->action_Show_comments->setChecked( TSchemaNodeModel::showComments );
    ui->action_Display_enums->setChecked(TSchemaNodeModel::useEnumNames);
    ui->action_Edit_id->setChecked(TSchemaNodeModel::editID );

    auto it = bsonuiWindows.begin();
    while( it != bsonuiWindows.end() )
    {  (*it)->updateViewMenu(); it++; }
}


/// Update model for all windows
void TBSONUITestWin::updateModel()
{
    auto it = bsonuiWindows.begin();
    while( it != bsonuiWindows.end() )
    {  (*it)->updateModel(); it++; }
}


/// Update table for all windows
void TBSONUITestWin::updateTable()
{
    auto it = bsonuiWindows.begin();
    while( it != bsonuiWindows.end() )
    {  (*it)->updateTable(); it++; }
}


/// Update DB for all windows
void TBSONUITestWin::updateDB()
{
    auto it = bsonuiWindows.begin();
    while( it != bsonuiWindows.end() )
    {  (*it)->updateDB(); it++; }
}

/// close all bson windows
void TBSONUITestWin::closeAll()
{
    auto it = bsonuiWindows.begin();
    while( it != bsonuiWindows.end() )
    {  (*it)->close(); it = bsonuiWindows.begin(); }

    // bsonuiWindows.clear();
}

void TBSONUITestWin::closeEvent(QCloseEvent* e)
{
   if( HelpMainWindow::pDia )
      delete HelpMainWindow::pDia;

   if( _csvWin )
    _csvWin->close();

   closeAll();

   QWidget::closeEvent(e);
}


void TBSONUITestWin::onDeleteEdge()
{
    auto it = bsonuiWindows.begin();
    while( it != bsonuiWindows.end() )
    {
       EdgesWidget* edgWin = dynamic_cast<EdgesWidget*>(*it);
       if( edgWin )
         edgWin->changeKeyList();
       it++;
    }
}

void TBSONUITestWin::onDeleteVertex()
{
    auto it = bsonuiWindows.begin();
    while( it != bsonuiWindows.end() )
    {
        EdgesWidget* edgWin = dynamic_cast<EdgesWidget*>(*it);
        if( edgWin )
          edgWin->updateAllKeys();
        else
          {
            VertexWidget* verWin = dynamic_cast<VertexWidget*>(*it);
            if( verWin )
              verWin->changeKeyList();
          }
        it++;
    }
}
