#ifndef TMAINWINDOW_H
#define TMAINWINDOW_H

#include <string>
#include <QMainWindow>
#include <QSettings>
using namespace std;

#include "thrift_schema.h"
#include "BSONUIBase.h"

namespace Ui {
class TBSONUITestWin;
}

namespace bsonui {
class  TableEditWidget;
}

class TBSONUITestWin : public QMainWindow
{
    Q_OBJECT

    // Internal data
    list<bsonui::BSONUIBase*> bsonuiWindows;
    bsonui::onEventfunction onCloseEvent;
    bsonui::ShowWidgetFunction showWidget;

    // Work functions
    void setActions();
    void closeEvent(QCloseEvent* e);

public slots:

    // update after change preferences
    void updateViewMenu();
    void updateModel();
    void updateTable();
    void updateDB();
    /// Close All BSONUIWidget windows
    void closeAll();

    // File
    //void CmReadSchema();
    void CmSetWorkDir();
    //void CmSetDBDir();
    void CmNewVertex();
    void CmNewEdge();
    void CmNewBson();
    void CmNewCsv();
    void CmNewImpexFormat();

    // Help
    void CmHelpAbout(){}
    void CmHelpAuthors(){}
    void CmHelpLicense(){}
    void CmHelpContens();

    void onDeleteVertex();
    void onDeleteEdge();

public:
    explicit TBSONUITestWin(QWidget *parent = 0);
    ~TBSONUITestWin();

    /// Open new EdgesWidget  or BSONUIWidget  window
    void OpenNewWidget( bool isVertex, const string& testschema,
                        const string& recordkey, const string& queryString="" );

private:

    Ui::TBSONUITestWin *ui;
    bsonui::TableEditWidget* _csvWin = 0;

};

#endif // TMAINWINDOW_H
