#include "BSONUITestMainWindow.h"
#include "preferences.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    bsonio::BsonioSettings::settingsFileName = "bsonui-test.json";
    QApplication a(argc, argv);
    TBSONUITestWin w;
    w.show();

    return a.exec();
}
